import math
from enum import Enum
from datetime import datetime, timedelta


class Aircraft:
    def __init__(self, pos_x, pos_y, pos_z, velocity, direction) -> None:
        self.fuel_level = 100  # fuel in %
        self.time_start = datetime.now()
        self.status = AircraftStatus(1)
        self.movement_frequency = 10  # Hz
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.pos_z = pos_z
        self.direction = direction  # (x,y,z), where sum of x,y,z=1
        self.nose = direction  # the plane's nose direction
        self.velocity = velocity  # m/s
        self.velocity_vector = (
            self.velocity * self.direction[0], self.velocity * self.direction[1], self.velocity * self.direction[2])  # (vx, vy, vz)

    def change_direction(self, target):
        vector = (target[0] - self.pos_x, target[1] -
                  self.pos_y, target[2] - self.pos_z)
        vector_value = math.sqrt(
            math.pow(vector[0], 2)+math.pow(vector[1], 2)+math.pow(vector[2], 2))
        new_direction = (vector[0]/vector_value,
                         vector[1]/vector_value, vector[2]/vector_value)
        self.direction = new_direction
        self.update_nose()
        return new_direction

    def move(self):
        time1 = datetime.now()
        time_diff = (datetime.now() - time1).total_seconds()
        while time_diff < 1/self.movement_frequency:
            time_diff = (datetime.now() - time1).total_seconds()

        self.update_velocity_vector()
        self.pos_x += self.velocity_vector[0]/self.movement_frequency
        self.pos_y += self.velocity_vector[1]/self.movement_frequency
        self.pos_z += self.velocity_vector[2]/self.movement_frequency

    def update_velocity_vector(self):
        self.velocity_vector = (
            self.velocity * self.direction[0], self.velocity * self.direction[1], self.velocity * self.direction[2])

    def decrease_fuel_level(self):
        time_passed = (datetime.now() - self.time_start).total_seconds()
        fuel_consumed = time_passed / (180*60)
        self.fuel_level -= fuel_consumed

    def update_nose(self):
        if self.status != AircraftStatus(7):
            self.nose = self.direction
        else:
            self.nose = (self.direction[0],
                         self.direction[1], -self.direction[2])


class AircraftStatus(Enum):
    APPROACHING_UPPER_BUFFER = 1
    INSIDE_UPPER_BUFFER = 2
    APPROACHING_LOWER_BUFFER = 3
    INSIDE_LOWER_BUFFER = 4
    APPROACHING_CORRIDOR = 5
    FOLLOWING_CORRIDOR = 6
    LANDING = 7


# airplane = Aircraft(0, 0, 0, 10, (0.5, 0.5, 0))
# print(airplane.status.name)
# for n in range(1, 11):
#     airplane.move()
#     if n == 5:
#         new_dir = airplane.change_direction((2.5, 2.5, 10))
#     airplane.decrease_fuel_level()
#     print(f"n: {n}, x: {airplane.pos_x}, y: {airplane.pos_y}, z: {airplane.pos_z}, direction: {airplane.direction}, time: {(datetime.now() - airplane.time_start).total_seconds()}, fuel: {airplane.fuel_level}")
