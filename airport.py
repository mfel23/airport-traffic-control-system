import threading


class Airport:
    def __init__(self) -> None:
        self.LENGTH = 10000
        self.WIDTH = 10000
        self.HEIGHT = 5000
        self.CORRIDOR_1 = [(100, 0), (100, 2000), (1600, 3000), (3100, 2000),
                           (3100, 0), (3100, -2000), (1600, -3000), (100, -2000)]
        self.CORRIDOR_2 = [(-100, 0), (-100, 2000), (-1600, 3000), (-3100, 2000),
                           (-3100, 0), (-3100, -2000), (-1600, -3000), (-100, -2000)]
        self.WAITING_ALTITUDE_1 = 2000
        self.WAITING_ALTITUDE_2 = 1500
        self.LANDING_START_ALTITUDE = 1000
        self.LANDING_PERMISSION_PLANE_Y = -2000

